#include"lud.h"
#include <pthread.h>
#ifndef FPGA_TARGET
#include <unistd.h>
#endif

#include <iostream>
#include <fstream>
#include <string.h>
#include <signal.h>

extern "C"{

  void lud(float* result, int size, float* ckpt_mem, int ckpt_id, int rate)
  {
    int i, j, k; 
    float sum;
    int init_i = 0;

    if(ckpt_id==1){
      i=int(ckpt_mem[0]);
      goto jump_label;
    }
    
    for (i=0; i<size; i++){
      for (j=i; j<size; j++){
        sum=result[i*size+j];
        for (k=0; k<i; k++) sum -= result[i*size+k]*result[k*size+j];
	result[i*size+j]=i*size+j;
      }

    jump_label:
      
      for (j=i+1;j<size; j++){
        sum=result[j*size+i];
        for (k=0; k<i; k++) sum -= result[j*size+k]*result[k*size+i];
	result[i*size+j]=i*size+j;
      }
    }
    return;
  }
  
  void workload(float* result, int size, float* ckpt_mem, int rate)
  {

#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem
#pragma HLS INTERFACE m_axi port=ckpt_mem offset=slave bundle=gmem
#pragma HLS INTERFACE s_axilite port=result bundle=control
#pragma HLS INTERFACE s_axilite port=size bundle=control
#pragma HLS INTERFACE s_axilite port=ckpt_mem bundle=control
#pragma HLS INTERFACE s_axilite port=rate bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

    printf("> workload: Starting workload %f\n", ckpt_mem[CKPT_ID]);
    
    int ckpt_id = ckpt_mem[CKPT_ID];
    lud(result, size, ckpt_mem, ckpt_id, rate);
    printf("> workload: isComplete=%f\n", ckpt_mem[COMPLETED]);
    return;
  }
}
