#ifndef LOCAL_SUPPORT_H
#define LOCAL_SUPPORT_H

#include"lud.h"

#define DEBUG_PRINT

extern volatile bool end_heartbeats;

int kernel_launcher(int initial_run, int fpga_target, const char * input_file, float * ckpt_mem = NULL, int failure_delay_ms=-1);
void* input_bench_to_data(const char* data_file_name);
  
void heatbeat_thread(const char* dest_ip, float* ckpt_mem, int size);
void backup_app(float * ckpt_mem, const char * input_file);

#endif
