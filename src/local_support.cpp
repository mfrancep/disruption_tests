#include "local_support.h"
#include "lud.h"
#include <string.h>
#include <fstream>
#include <iostream>
#include <unistd.h>    /* standard unix functions, like alarm()          */
#include <signal.h>
#include <thread>
#include <stdexcept>
#include <sys/types.h>
#include <vector>
#include <functional>

#include <sys/mman.h>
#include <stdlib.h>

#define _POSIX_SOURCE
#include <sys/wait.h>

#ifdef FPGA_TARGET
// XRT includes
#include "xrt/xrt_bo.h"
#include <experimental/xrt_xclbin.h>
#include "xrt/xrt_device.h"
#include "xrt/xrt_kernel.h"
#endif

#define METADATA_NUM 4
#define CKPT_SIZE 4 

#define HEARTBEAT_PERIOD_US 100000 //100ms


extern "C" int workload(float *result, int size, float *ckpt_mem, int rate);

float mem_ckpt[CKPT_SIZE];
float mem_ckpt_ref[CKPT_SIZE];

int size = 1024;

float* result = NULL;

#ifdef FPGA_TARGET
xrt::bo ckpt_buffer;
xrt::bo heartbeat_buffer;
#endif

int arrToFile(float* arr, int arrSize, std::string filename) {
  std::ofstream myfile (filename);
  if (myfile.is_open()) {
    for(int i=0; i < arrSize; i ++) {
      myfile << arr[i] << "\n";
    }
    myfile.close();
    return 1;
  }
  else std::cout << "Unable to open file";
  return 0;
}


int main(int argc, char** argv) {

  std::thread killer_tid;
  int ckpt_rate = 1;
  std::cout << "argc = " << argc << std::endl;

  for(int i=1; i < argc; i++){
    if(strcmp("--rate", argv[i]) == 0){
      if((i+1)>argc)
	printf("Ckpt rate\n");
      else
	ckpt_rate = atoi(argv[i+1]);
    }
  }

  // Read settings
  std::string binaryFile = "./xclbin/lud.hw.xilinx_u50_gen3x16_xdma_201920_3.xclbin";
  int device_index = 0;

  std::cout << "Open the device" << device_index << std::endl;
  auto device = xrt::device(device_index);
  std::cout << "Load the xclbin " << binaryFile << std::endl;
  auto uuid = device.load_xclbin(binaryFile);

  auto krnl = xrt::kernel(device, uuid, "workload", xrt::kernel::cu_access_mode::exclusive);

  printf("size %d\n", size);

  std::cout << "Allocate Buffer in Global Memory" << std::endl;
  auto result_buffer = xrt::bo(device, size*size*sizeof(float), krnl.group_id(0)); //Match kernel arguments to RTL kernel
  ckpt_buffer = xrt::bo(device, CKPT_SIZE*sizeof(float), krnl.group_id(2));
  
  std::cout << "XRT Buffer ok\n";

  result = (float *) malloc(size*size*sizeof(float));

  memset(result, 0, size*size*sizeof(float));
  memset(mem_ckpt, 0, CKPT_SIZE*sizeof(float));

  printf("Start Kernel computation\n");

  mem_ckpt[0] = 0; //1024/2;
  mem_ckpt[CKPT_ID] = 0; //1
  
  float * ref_result = (float*) malloc(size*size*sizeof(float));
  memcpy(ref_result, result, size*size*sizeof(float));
  //Compute ref outputs
  int ref_completion = workload(ref_result, size, mem_ckpt, ckpt_rate);

  printf("Start Kernel computation\n");

  // 0th: initialize the timer at the beginning of the program
  
  // Write our data set into device buffers
  result_buffer.write(result);
  result_buffer.sync(XCL_BO_SYNC_BO_TO_DEVICE);
  ckpt_buffer.write(mem_ckpt);
  ckpt_buffer.sync(XCL_BO_SYNC_BO_TO_DEVICE);
  
  // Execute the kernel over the entire range of our 1d input data set
  // using the maximum number of work group items for this device
  //
  
  std::cout << "Execution of the kernel\n";
  auto run = krnl(result_buffer, size, ckpt_buffer, ckpt_rate);
  run.wait();
  
  // 4th: time of kernel execution

  // Read back the results from the device to verify the output
  result_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
  result_buffer.read(result);
  ckpt_buffer.sync(XCL_BO_SYNC_BO_FROM_DEVICE);
  ckpt_buffer.read(mem_ckpt);
  // 5th: time of data retrieving (PCIe + memcpy)

  arrToFile(ref_result, size*size, "ref_result.txt");
  arrToFile(result, size*size, "final_result.txt");
    
  printf("\n free memory\n");
  free(result);
}
