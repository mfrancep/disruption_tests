.PHONY: help

help::
	$(ECHO) "Makefile Usage:"
	$(ECHO) "  make all TARGET=<sw_emu/hw_emu/hw> DEVICE=<FPGA platform>"
	$(ECHO) "      Command to generate the design for specified Target and Device."
	$(ECHO) ""
	$(ECHO) "  make clean "
	$(ECHO) "      Command to remove the generated non-hardware files."
	$(ECHO) ""
	$(ECHO) "  make cleanall"
	$(ECHO) "      Command to remove all the generated files."
	$(ECHO) ""
	$(ECHO) "  make check TARGET=<sw_emu/hw_emu/hw> DEVICE=<FPGA platform>"
	$(ECHO) "      Command to run application in emulation."
	$(ECHO) ""
	$(ECHO) "  make build TARGET=<sw_emu/hw_emu/hw> DEVICE=<FPGA platform>"
	$(ECHO) "      Command to build xclbin application."
	$(ECHO) ""
	$(ECHO) "  make run_nimbix DEVICE=<FPGA platform>"
	$(ECHO) "      Command to run application on Nimbix Cloud."
	$(ECHO) ""
	$(ECHO) "  make aws_build DEVICE=<FPGA platform>"
	$(ECHO) "      Command to build AWS xclbin application on AWS Cloud."
	$(ECHO) ""

# Compiler tools
# XILINX_SDX ?= /local-scratch/SDx/SDx/2019.1
# XILINX_XRT ?= /opt/xilinx/xrt
# XILINX_SDK ?= $(XILINX_SDX)/../../SDK/2019.1
# XILINX_VIVADO ?= /local-scratch/SDx/Vivado/2019.1
# XILINX_VIVADO_HLS ?= $(XILINX_SDX)/Vivado_HLS

VIVADO:=vivado
DIR_PRJ = ./proj
SCRIPTS_DIR = ./scripts
HDLSRCS= $(DIR_PR)/kernel.xml

# Kernal Name
APP=lud
KERNEL_NAME=workload
CASE=4

KERNEL_CPU_O=kernel.o 

# Points to Utility Directory
COMMON_REPO = ../common
ABS_COMMON_REPO = $(shell readlink -f $(COMMON_REPO))

TARGETS := hw
TARGET := $(TARGETS)
DEVICES := xilinx_u50_gen3x16_xdma_201920_3
DEVICE := $(DEVICES)
XCLBIN := ./xclbin

include ./utils.mk

DSA := $(call device2dsa, $(DEVICE))
BUILD_DIR := ./_x.$(TARGET).$(DSA)

BUILD_DIR_KERNEL = $(BUILD_DIR)/$(APP)

CXX := g++
VPP := $(XILINX_VITIS)/bin/v++

#Include Libraries
host_CXXFLAGS += -g -I./ -I$(XILINX_XRT)/include -I$(XILINX_VIVADO)/include -Wall -O0 -g -std=c++17 -DFPGA_TARGET
CXXFLAGS += $(host_CXXFLAGS)
krn_CXXFLAGS += -g -Wall -O0 -g -std=c++17 -DCPU_VERSION
xrt_LDFLAGS += -L$(XILINX_XRT)/lib -lxrt_coreutil -pthread
LDFLAGS += $(xrt_LDFLAGS)

HOST_SRCS += src/local_support.cpp
HOST_HDRS +=  
HOST_ARGS = 

KERNEL_SRCS = src/case$(CASE).cpp 

OBJ = ./src/host.o $(KERNEL_CPU_O)

# Host compiler global settings
CXXFLAGS += -fmessage-length=0 -DSDX_PLATFORM=$(DEVICE) -D__USE_XOPEN2K8 -I$(XILINX_XRT)/include/ -I$(XILINX_VIVADO)/include/
LDFLAGS += -lrt -lstdc++ 

# Kernel compiler global settings
CLFLAGS += -t $(TARGET) --platform $(DEVICE) --save-temps

EXECUTABLE = host
CMD_ARGS = $(XCLBIN)/$(APP).$(TARGET).$(DSA).xclbin

EMCONFIG_DIR = $(XCLBIN)/$(DSA)

BINARY_CONTAINERS += $(XCLBIN)/$(APP).$(TARGET).$(DSA).xclbin
BINARY_CONTAINER_vadd_OBJS += $(XCLBIN)/$(APP).$(TARGET).$(DSA).xo

CP = cp -rf

.PHONY: all clean cleanall docs emconfig
all: check-devices $(EXECUTABLE) $(BINARY_CONTAINERS) emconfig

.PHONY: exe
exe: $(EXECUTABLE)

.PHONY: build
build: $(BINARY_CONTAINERS)

# Building kernel
$(XCLBIN)/$(APP).$(TARGET).$(DSA).xo: src/case$(CASE).cpp #src/$(APP).cpp
	mkdir -p $(XCLBIN)
	$(VPP) $(CLFLAGS) --temp_dir $(BUILD_DIR_KERNEL) -c -k $(KERNEL_NAME) -I'$(<D)' -o'$@' '$<'
$(XCLBIN)/$(APP).$(TARGET).$(DSA).xclbin: $(BINARY_CONTAINER_vadd_OBJS)
	mkdir -p $(XCLBIN)
	$(VPP) $(CLFLAGS) --temp_dir $(BUILD_DIR_KERNEL) -R2 -l $(LDCLFLAGS) --connectivity.nk $(KERNEL_NAME):1 -j 8 -o'$@' $(+) $(XOCC_LINK_OPTS)

$(KERNEL_CPU_O): $(KERNEL_SRCS)
	$(CXX) $(krn_CXXFLAGS) -c $(KERNEL_SRCS) -o '$@'

src/host.o: $(HOST_SRCS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

# Building Host
$(EXECUTABLE): $(HOST_HDRS) $(OBJ)
	$(CXX) -o '$@' $(OBJ) $(LDFLAGS)

emconfig:$(EMCONFIG_DIR)/emconfig.json
$(EMCONFIG_DIR)/emconfig.json:
	emconfigutil --platform $(DEVICE) --od $(EMCONFIG_DIR)

check: all
ifeq ($(TARGET),$(filter $(TARGET),sw_emu hw_emu))
	$(CP) $(EMCONFIG_DIR)/emconfig.json .
	XCL_EMULATION_MODE=$(TARGET) ./$(EXECUTABLE) $(HOST_ARGS) $(XCLBIN)/$(APP).$(TARGET).$(DSA).xclbin
else
	 ./$(EXECUTABLE) $(HOST_ARGS) $(XCLBIN)/$(APP).$(TARGET).$(DSA).xclbin
endif

run_nimbix: all
	$(COMMON_REPO)/utility/nimbix/run_nimbix.py $(EXECUTABLE) $(CMD_ARGS) $(DSA)

aws_build: check-aws_repo $(BINARY_CONTAINERS)
	$(COMMON_REPO)/utility/aws/run_aws.py $(BINARY_CONTAINERS)

# Cleaning stuff
clean:
	-$(RMDIR) $(EXECUTABLE) $(XCLBIN)/{*sw_emu*,*hw_emu*} 
	-$(RMDIR) profile_* TempConfig system_estimate.xtxt *.rpt *.csv 
	-$(RMDIR) src/*.ll _xocc_* .Xil emconfig.json dltmp* xmltmp* *.log *.jou *.wcfg *.wdb *.o src/*.o *.json

cleanall: clean
	-$(RMDIR) $(XCLBIN)
	-$(RMDIR) _x.* output.data *.run_summary .run
	-$(RMDIR) ./packaged_kernel ./proj ./tmp_kernel_pack
